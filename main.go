package main

import (
	"log"

	"gitlab.com/wicak/example-go-pkg/v3/color"
	"gitlab.com/wicak/example-go-pkg/v3/food"
	"gitlab.com/wicak/example-go-pkg/v3/halalfood"
)

func main() {
	foods := food.List

	log.Println("daftar makanan")
	for key, f := range foods {
		log.Println(key, f)
	}

	log.Println("daftar warna")
	for key, c := range color.List {
		log.Println(key, c)
	}

	log.Println("daftar makanan halal")
	for key, c := range halalfood.List {
		log.Println(key, c)
	}
}
