module gitlab.com/wicak/example-go

go 1.20

require gitlab.com/wicak/example-go-pkg/v3 v3.0.0

require gitlab.com/wicak/example-go-pkg/v4 v4.0.2-0.0.1 // indirect
